import React from 'react';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      items: [],
      title: 'Click to Fetch'
    }
    this.update = this.update.bind(this);
  }

  update() {
    this.setState({title: 'Fetching...'})
    fetch('https://swapi.co/api/people/?format=json').then(response => response.json()).then(({results: items}) => this.setState({items: items, title: 'Done'}))
  }

  render() {
    let items = this.state.items
    return (
      <div>
        <h2>{this.state.title}</h2>
        <Button update={this.update}/> {items.map(item => <Widget name={item.name}/>)}
      </div>
    )
  }
}

const Widget = (props) => <h3>{props.name}</h3>
const Button = (props) => <button onClick={props.update}>Click Me</button>

export default App;
